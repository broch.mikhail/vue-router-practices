import Vue from "vue";
import Vuex from "vuex";
Vue.use(Vuex);
export default new Vuex.Store({
  state: {
    user: {
      loggedIn: true, //  authenticated users
      isSubscribed: true // subscribed users
    }
  },
  getters: {
    auth(state) {
      return state.user;
    }
  }
});
